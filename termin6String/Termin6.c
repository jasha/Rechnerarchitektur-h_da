// C-Programm um die in Assembler geschriebene Funktion zu testen.
// Das Programm kann/sollte für weitere Tests erweitert werden.
// von: Manfred
// vom: 16.02.2011 

#include <stdio.h>
//int searchStringInString(char*, char*);

int main (void)
{
	int Anzahl;
	char* Text="hallo Alles klar?";
	//char* Text="naaa waaa gaahts dir aaalter saaack?";
	char* Suchstring="ll";
	//char* Suchstring="aaa";
	//char* Text="annanna";
	//char* Suchstring="anna";
	char* Leerstring="";
	
	
	Anzahl = searchStringInString(Text, Suchstring);
	printf("Anzahl: %d", Anzahl);
	
	Anzahl = searchStringInString(Text, Leerstring);
	printf("Anzahl: %d", Anzahl);	
	
	Anzahl = searchStringInString(Leerstring, Suchstring);
	printf("Anzahl: %d", Anzahl);
	
	Anzahl = searchStringInString(Leerstring, Leerstring);
	printf("Anzahl: %d", Anzahl);
	
// Aufrufe funktionieren auch so..
//	Anzahl = searchStringInString("aabaabaab", "ab");

//Anzahl = searchStringInString("annanna", "anna");
Anzahl = searchStringInString("lllla", "llla");
Anzahl = searchStringInString("aaaaab", "aaa");

	return 0;
}
